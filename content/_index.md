---
layout: "index"
framed: true
date: 2020-12-10T12:51:43-03:00
draft: false
---

# Hello great people of the world!
Welcome to my personal website! Below you will see a list of posts I have
made. Here you can find thoughts about my career and how I observe and
understand life.