---
title: "About"
date: 2020-12-09T12:33:29-03:00
draft: false
author: Ederson Ferreira
authorTwitter: "edersonmf"
cover: https://s.gravatar.com/avatar/9f24ba1b093af97aec203c8849fa56a6?s=200
tags: ["creator"]
---
I have 20+ years in the software development industry and 10+ years of technical leadership & management experience, especially in distributed environments.

My extensive experience, training and education coupled with particular skills make me able to deal very effectively with technical issues and problems of different natures. I am driven by the pursuit of excellence and curiosity towards a continuous improvement lifestyle.

I lead by example. I am naturally uncomfortable with the status quo. I never settle for skills that my team already possesses. Instead, I continuously seek to build a team full of members in potential or members that are already very good at what they do. Developing people's skill set is one of the activities I am joyful at the most.

I have experience in designing and architecture highly-scalable and cloud native solutions, starting from efficient messaging platforms, real-time group chat, push notifications, REST and gRPC based APIs. Individual contributor, I have the ability to work with multicultural teams around the globe to deliver results.

Architect various complex systems to achieve high-availability, high throughput making them ease to manage and deploy. Work with some of the largest companies in Brazil and the US helping them to create the best solutions and products. Technical management of a high-performance development team. 

